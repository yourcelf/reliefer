#!/usr/bin/env python
import os
import glob
import subprocess

#bounds = [45, -111.5, 46, -110.5] # Just Bozeman
#states = "montana"
#bounds = [42, -112, 46.5, -108] # Bozeman/yellowstone/surrounds
#states = "montana"
bounds = [35, -115, 49, -106] # The west
states = "montana utah idaho arizona colorado new-mexico"


def go(*args, **kwargs):
    os.chdir(os.path.dirname(__file__))
    kwargs['shell'] = kwargs.get('shell', True)
    subprocess.check_call(*args, **kwargs)

go("run/00-download-hgts {0} {1} {2} {3}".format(*bounds))
go("run/10-generate-contours {0} {1} {2} {3}".format(*bounds))
go("run/11-contours-to-postgis {0} {1} {2} {3}".format(*bounds))
go("run/20-generate-hillshades {0} {1} {2} {3}".format(*bounds))
go("run/21-merge-hillshades {0} {1} {2} {3}".format(*bounds))
go("run/30-fetch-osm montana")
go("run/30-osm-to-postgis osm_data/sorted.osm")

#go("cd {mapnik} ; rm -r tiles/* ; ./polytiles.py "
#    "--bbox {1} {0} {3} {2} -z 6 15 --threads 8".format(
#            *bounds, mapnik=os.path.join(os.path.dirname(__file__), "mapnik")
#))
