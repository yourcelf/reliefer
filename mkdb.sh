#!/bin/bash

# Creates database superuser 'osm' with database named 'osm' which is set up as
# needed for Open Street Maps.  Semantics for postgresql 9.1, as installed by
# brew under OS X.
#
# Run as database superuser.

createuser -s osm
dropdb osm
createdb -O osm osm
createlang plpgsql osm
psql -U osm -d osm -c "CREATE EXTENSION hstore;" && \
psql -U osm -d osm -f /usr/local/share/postgis/postgis.sql  && \
psql -U osm -d osm -f /usr/local/share/postgis/spatial_ref_sys.sql && \
psql -U osm -d osm -f /usr/local/Cellar/osmosis/0.38/libexec/script/pgsql_simple_schema_0.6.sql



