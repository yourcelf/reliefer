#!/usr/bin/env python

import os
import subprocess
from math import floor, ceil
import logging

outdir = os.path.join(os.path.dirname(__file__), "..", "elevation_data", "contours")
indir = os.path.join(os.path.dirname(__file__), "..", "elevation_data", "hgt")
INTERVAL = 10 # meters

def run(bounds):
    lls = []
    logging.info("Generating contours for %s" % bounds)
    for lat in range(int(floor(bounds[0])), int(ceil(bounds[2]))):
        for lng in range(int(floor(bounds[1])), int(ceil(bounds[3]))):
            lls.append((lat, lng))

    names = ["N{lat}W{lng}.hgt".format(lat=ll[0], lng=abs(ll[1])) for ll in lls]
    files = [os.path.join(indir, name) for name in names]

    try:
        os.makedirs(outdir)
    except OSError:
        pass

    for i,name in enumerate(files):
        logging.debug("%s, %s/%s" % (name, i+1, len(files)))
        base = os.path.splitext(os.path.basename(name))[0]
        shp = os.path.join(outdir, "%s.shp" % base)
        for ext in (".shp", ".dbf", ".prj", ".shx"):
            try:
               os.remove(os.path.splitext(shp)[0] + ext)
            except OSError:
                pass
        subprocess.check_call("gdal_contour "
                "-i {interval} -snodata 32767 -a height "
                "{name} {shp}".format(interval=INTERVAL, name=name, shp=shp),
            shell=True)
    logging.info("done." % bounds)

if __name__ == "__main__":
    import sys
    logging.basicConfig(level=logging.DEBUG)
    bounds = [float(a) for a in sys.argv[1:]]
    run(bounds)
